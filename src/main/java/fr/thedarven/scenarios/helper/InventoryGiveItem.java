package fr.thedarven.scenarios.helper;

import org.bukkit.entity.Player;

public interface InventoryGiveItem {

    /**
     * Pour donner les items de à un joueur
     *
     * @param player Le joueur
     */
    void giveItems(Player player);

}
